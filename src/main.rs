use app_manager::generator::helm::generate_chart;
use app_manager::generator::kubernetes::generator::generate_kubernetes_config;
use app_manager::generator::kubernetes::generator::ingress::get_app_ingress;
use app_manager::instance_type::InstanceType;
use app_manager::manage::files::write_stores_yml;
use app_manager::repos::types::AppStore;
use k8s_crds_traefik::IngressRoute;
use kube::api::PostParams;
use kube::{Api, Client};
use rand::Rng;
use std::collections::BTreeMap;
use std::str::FromStr;

mod kubernetes;

mod disk;
mod sys_components;

use crate::kubernetes::apply::apply_with_ns;
use crate::kubernetes::{create_namespace, create_secret};
use crate::sys_components::builtins::*;
use crate::sys_components::longhorn;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    let kube_client = Client::try_default()
        .await
        .expect("Failed to create kube client");
    let instance_type =
        InstanceType::from_str(&std::env::var("INSTANCE_TYPE").unwrap_or("home".to_string()))
            .expect("Failed to parse instance type");
    let has_longhorn = longhorn::is_installed(&kube_client)
        .await
        .expect("Failed to check for Longhorn");
    if !has_longhorn {
        let longhorn_version = longhorn::get_latest_release(semver::Version::new(1, 5, 3))
            .await
            .expect("Failed to get latest Longhorn version");
        tracing::info!("Installing Longhorn version {}", longhorn_version);
        longhorn::install(&kube_client, &longhorn_version)
            .await
            .expect("Failed to install Longhorn");
    }

    let has_cert_manager = sys_components::cert_manager::is_installed(&kube_client)
        .await
        .expect("Failed to check for cert-manager");
    if !has_cert_manager {
        let cert_manager_version =
            sys_components::cert_manager::get_latest_release(semver::Version::new(1, 13, 3))
                .await
                .expect("Failed to get latest cert-manager version");
        tracing::info!("Installing cert-manager version {}", cert_manager_version);
        sys_components::cert_manager::install(&kube_client, &cert_manager_version)
            .await
            .expect("Failed to install cert-manager");
    }

    let has_upgrade_controller =
        sys_components::system_upgrade_controller::is_installed(&kube_client)
            .await
            .expect("Failed to check for system-upgrade-controller");
    if !has_upgrade_controller {
        let mut upgrade_controller_version =
            sys_components::system_upgrade_controller::get_latest_release(semver::Version::new(
                0, 13, 2,
            ))
            .await
            .expect("Failed to get latest system-upgrade-controller version");
        if upgrade_controller_version == semver::Version::new(0, 13, 3) {
            // This specific version has a bug that causes it to fail to install
            upgrade_controller_version = semver::Version::new(0, 13, 2);
        }
        tracing::info!(
            "Installing system-upgrade-controller version {}",
            upgrade_controller_version
        );
        create_namespace(kube_client.clone(), "system-upgrade")
            .await
            .expect("Failed to create namespace for system-upgrade-controller");
        sys_components::system_upgrade_controller::install(
            &kube_client,
            &upgrade_controller_version,
        )
        .await
        .expect("Failed to install system-upgrade-controller");
    }
    if instance_type == InstanceType::Lite {
        // Lite only supports one, primary disk
        longhorn::set_default_replica_count(&kube_client, 1)
            .await
            .expect("Failed to set number of replicas!");
    } else {
        longhorn::enable_v2_data_engine(&kube_client)
            .await
            .expect("Failed to enable v2 data engine!");
        let disks = disk::initialize_disks().expect("Failed to initialize disks!");
        if disks.is_empty() {
            tracing::error!("No disks found!");
            std::process::exit(1);
        }
        loop {
            if let Err(err) = longhorn::remove_all_disks(&kube_client).await {
                tracing::warn!("Failed to remove all disks: {:#?}", err);
                tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
                continue;
            }
            if let Err(err) = longhorn::add_disks(&kube_client, &disks).await {
                tracing::warn!("Failed to add disks: {:#?}", err);
                tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
                continue;
            }
            if let Err(err) =
                longhorn::set_default_replica_count(&kube_client, disks.len() as u8).await
            {
                tracing::warn!("Failed to set number of replicas: {:#?}", err);
                tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
                continue;
            }
            break;
        }
    }
    longhorn::set_setting(
        &kube_client,
        "node-drain-policy",
        "allow-if-replica-is-stopped",
    )
    .await
    .expect("Failed to set drain policy!");
    longhorn::set_setting(&kube_client, "storage-over-provisioning-percentage", "1000")
        .await
        .expect("Failed to set over-provisioning policy!");
    longhorn::set_setting(&kube_client, "replica-auto-balance", "best-effort")
        .await
        .expect("Failed to set replica auto-balance policy!");

    let mut rng = rand::thread_rng();
    let mut entropy = [0u8; 64];
    rng.fill(&mut entropy);
    let nirvati_seed = hex::encode(entropy);
    create_secret(
        &kube_client,
        "admin-nirvati",
        "nirvati-seed",
        &BTreeMap::from([("seed".to_string(), nirvati_seed.clone())]),
    )
    .await
    .expect("Failed to create secret!");
    let ingress_longhorn = [longhorn_ingress()];
    let middlewares_nirvati = [compress_mw(), https_redirect_mw()];
    let middlewares_longhorn = [strip_longhorn_prefix_mw()];
    let helmconfigs_cluster = [traefik_helm_config()];

    apply_with_ns(kube_client.clone(), &ingress_longhorn, "longhorn-system")
        .await
        .expect("Failed to apply longhorn ingress!");
    apply_with_ns(kube_client.clone(), &middlewares_nirvati, "nirvati")
        .await
        .expect("Failed to apply middlewares!");
    apply_with_ns(
        kube_client.clone(),
        &middlewares_longhorn,
        "longhorn-system",
    )
    .await
    .expect("Failed to apply middlewares (longhorn)!");
    if let Err(e) = apply_with_ns(kube_client.clone(), &helmconfigs_cluster, "kube-system").await {
        // TODO: Automatically configure Traefik on non-K3s clusters
        tracing::warn!(
            "Failed to apply helm configs (These are only required on K3s, so feel free to ignore this on other Kubernetes implementations): {:#?}",
            e
        );
    }
    let fake_apps_root = temp_dir::TempDir::new().expect("Failed to create temp dir");
    let init_stores_yml = vec![AppStore {
        src: "https://gitlab.com/nirvati/apps/essentials#main".to_string(),
        ..Default::default()
    }];

    write_stores_yml(fake_apps_root.path(), &init_stores_yml).expect("Failed to write stores.yml");
    app_manager::repos::download_apps(fake_apps_root.path(), false, &[])
        .expect("Failed to download apps");
    if std::env::var("IS_DEVELOPMENT").is_err() {
        let nirvati_app = app_manager::parser::load_app(
            fake_apps_root.path(),
            "nirvati",
            "admin",
            &[],
            &nirvati_seed,
            Default::default(),
            instance_type,
        )
        .await
        .expect("Failed to load Nirvati app");
        let metadata = nirvati_app.get_metadata().clone();
        let ingress = get_app_ingress(
            None,
            nirvati_app.get_ingress(),
            vec!["web".to_string()],
            false,
            "admin",
        );
        let kube_config =
            generate_kubernetes_config(&kube_client, nirvati_app, "admin".to_string())
                .await
                .expect("Failed to generate Kubernetes config");
        let helm_chart = generate_chart(kube_config, metadata, "admin")
            .expect("Failed to turn app into Helm chart");
        let chart_dir = temp_dir::TempDir::new().expect("Failed to create temp dir");
        let chart_file_path = chart_dir.path().join("chart.tgz");
        std::fs::write(&chart_file_path, helm_chart).expect("Failed to write chart to file");
        let helm_install = tokio::process::Command::new("helm")
            .args([
                "install",
                "nirvati",
                chart_file_path
                    .to_str()
                    .expect("Failed to convert chart file path to string"),
                "--namespace",
                "admin-nirvati",
                "--create-namespace",
            ])
            .output()
            .await
            .expect("Failed to run helm install");
        if !helm_install.status.success() {
            tracing::error!(
                "Failed to install Nirvati app: {}",
                String::from_utf8_lossy(&helm_install.stderr)
            );
            std::process::exit(1);
        }
        let ingress_api: Api<IngressRoute> = Api::namespaced(kube_client.clone(), "admin-nirvati");
        let pp = PostParams::default();
        ingress_api.create(&pp, &ingress).await.unwrap();
    } else {
        tracing::info!("Skipping Nirvati app installation due to development mode");
    }
    let chartmuseum_app = app_manager::parser::load_app(
        fake_apps_root.path(),
        "chartmuseum",
        "admin",
        &[],
        &nirvati_seed,
        Default::default(),
        instance_type,
    )
    .await
    .expect("Failed to load Chartmuseum app");
    let metadata = chartmuseum_app.get_metadata().clone();
    let kube_config =
        generate_kubernetes_config(&kube_client, chartmuseum_app, "admin".to_string())
            .await
            .expect("Failed to generate Kubernetes config");
    let helm_chart =
        generate_chart(kube_config, metadata, "admin").expect("Failed to turn app into Helm chart");
    let chart_dir = temp_dir::TempDir::new().expect("Failed to create temp dir");
    let chart_file_path = chart_dir.path().join("chart.tgz");
    std::fs::write(&chart_file_path, helm_chart).expect("Failed to write chart to file");
    let helm_install = tokio::process::Command::new("helm")
        .args([
            "install",
            "chartmuseum",
            chart_file_path
                .to_str()
                .expect("Failed to convert chart file path to string"),
            "--namespace",
            "admin-chartmuseum",
            "--create-namespace",
        ])
        .output()
        .await
        .expect("Failed to run helm install");
    if !helm_install.status.success() {
        tracing::error!(
            "Failed to install Chartmuseum app: {}",
            String::from_utf8_lossy(&helm_install.stderr)
        );
        std::process::exit(1);
    }
}
