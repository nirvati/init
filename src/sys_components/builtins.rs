use k8s_openapi::apimachinery::pkg::util::intstr::IntOrString;

use k8s_crds_helm_controller::*;
use k8s_crds_traefik::*;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;

pub fn compress_mw() -> Middleware {
    Middleware {
        metadata: k8s_meta::ObjectMeta {
            name: Some("compress".to_string()),
            namespace: Some("nirvati".to_string()),
            ..Default::default()
        },
        spec: MiddlewareSpec {
            compress: Some(MiddlewareCompress {
                ..Default::default()
            }),
            ..Default::default()
        },
    }
}

pub fn https_redirect_mw() -> Middleware {
    Middleware {
        metadata: k8s_meta::ObjectMeta {
            name: Some("https-redirect".to_string()),
            namespace: Some("nirvati".to_string()),
            ..Default::default()
        },
        spec: MiddlewareSpec {
            redirect_scheme: Some(MiddlewareRedirectScheme {
                permanent: Some(true),
                scheme: Some("https".to_string()),
                ..Default::default()
            }),
            ..Default::default()
        },
    }
}

pub fn longhorn_ingress() -> IngressRoute {
    IngressRoute {
        metadata: k8s_meta::ObjectMeta {
            name: Some("longhorn-ui".to_string()),
            namespace: Some("longhorn-system".to_string()),
            ..Default::default()
        },
        spec: IngressRouteSpec {
            entry_points: Some(vec!["web".to_string()]),
            routes: vec![IngressRouteRoutes {
                r#match: "PathPrefix(`/longhorn`)".to_string(),
                kind: IngressRouteRoutesKind::Rule,
                priority: Some(1),
                services: Some(vec![IngressRouteRoutesServices {
                    name: "longhorn-frontend".to_string(),
                    port: Some(IntOrString::Int(80)),
                    kind: Some(IngressRouteRoutesServicesKind::Service),
                    ..Default::default()
                }]),
                middlewares: Some(vec![
                    IngressRouteRoutesMiddlewares {
                        name: "auth-admin".to_string(),
                        namespace: Some("nirvati".to_string()),
                    },
                    IngressRouteRoutesMiddlewares {
                        name: "strip-longhorn-prefix".to_string(),
                        ..Default::default()
                    },
                ]),
            }],

            ..Default::default()
        },
    }
}

pub fn strip_longhorn_prefix_mw() -> Middleware {
    Middleware {
        metadata: k8s_meta::ObjectMeta {
            name: Some("strip-longhorn-prefix".to_string()),
            namespace: Some("longhorn-system".to_string()),
            ..Default::default()
        },
        spec: MiddlewareSpec {
            strip_prefix: Some(MiddlewareStripPrefix {
                prefixes: Some(vec!["/longhorn".to_string()])
            }),
            ..Default::default()
        },
    }
}

pub fn traefik_helm_config() -> HelmChartConfig {
    HelmChartConfig {
        metadata: k8s_meta::ObjectMeta {
            name: Some("traefik".to_string()),
            namespace: Some("kube-system".to_string()),
            ..Default::default()
        },
        spec: HelmChartConfigSpec {
            values_content: Some(
                r#"globalArguments:
- "--providers.kubernetescrd.allowCrossNamespace=true"
- "--providers.kubernetescrd.allowExternalNameServices=true"
"#
                .to_string(),
            ),
            ..Default::default()
        },
    }
}
