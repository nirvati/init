use anyhow::Result;
use kube::api::{Patch, PatchParams};
use kube::{Api, Client};
use semver::Version;

use crate::kubernetes::apply::apply_any;
use crate::kubernetes::deserialize::multidoc_deserialize;
use crate::sys_components::shared::get_latest_release_from_repo_semver;
use k8s_crds_longhorn::*;
use k8s_openapi::api::core::v1 as k8s;
use k8s_openapi::api::storage::v1 as k8s_storage;
use slugify::slugify;

pub async fn get_latest_release(installed_version: Version) -> Result<Version> {
    get_latest_release_from_repo_semver(
        Some(installed_version),
        "longhorn".to_string(),
        "longhorn".to_string(),
    )
    .await
}

pub async fn is_installed(client: &Client) -> Result<bool> {
    // Check whether the cert-manager namespace exists
    let api: Api<k8s_openapi::api::core::v1::Namespace> = Api::all(client.clone());
    let namespaces = api.list(&Default::default()).await?;
    let has_ns = namespaces
        .iter()
        .any(|ns| ns.metadata.name == Some("longhorn-system".into()));
    Ok(has_ns)
}

pub async fn install(client: &Client, _version: &Version) -> Result<()> {
    let response = reqwest::get(
        "https://raw.githubusercontent.com/longhorn/longhorn/v1.6.0-rc3/deploy/longhorn.yaml",
    )
    .await?;
    let body = response.text().await?;
    let latest_config = multidoc_deserialize(&body)?;

    apply_any(client.clone(), latest_config).await?;

    // Wait for the longhorn-storageclass configmap and the longhorn-system namespace to be created
    loop {
        let api: Api<k8s_openapi::api::core::v1::Namespace> = Api::all(client.clone());
        let namespaces = api.list(&Default::default()).await;
        if let Ok(namespaces) = namespaces {
            let has_ns = namespaces
                .iter()
                .any(|ns| ns.metadata.name == Some("longhorn-system".into()));
            if has_ns {
                break;
            }
        }
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
    }
    loop {
        let api: Api<k8s_openapi::api::core::v1::ConfigMap> =
            Api::namespaced(client.clone(), "longhorn-system");
        let configmaps = api.list(&Default::default()).await;
        if let Ok(configmaps) = configmaps {
            let has_cm = configmaps
                .iter()
                .any(|cm| cm.metadata.name == Some("longhorn-storageclass".into()));
            if has_cm {
                break;
            }
        }
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
    }
    Ok(())
}

pub async fn set_default_replica_count(client: &Client, replicas: u8) -> Result<()> {
    let pp = PatchParams {
        force: true,
        field_manager: Some("nirvati".to_string()),
        ..Default::default()
    };
    {
        set_setting(client, "default-replica-count", &replicas.to_string()).await?;
    }
    {
        let config_api: Api<k8s::ConfigMap> = Api::namespaced(client.clone(), "longhorn-system");
        let mut config = config_api.get("longhorn-storageclass").await?;
        config.metadata.managed_fields = None;
        let storage_class_data = config
            .data
            .clone()
            .ok_or(anyhow::anyhow!("No data in configmap"))?;
        let storage_class_yml = storage_class_data
            .get("storageclass.yaml")
            .ok_or(anyhow::anyhow!("No storageclass.yaml in configmap"))?;
        let mut storage_class_yml: serde_yaml::Value = serde_yaml::from_str(storage_class_yml)?;
        storage_class_yml
            .get_mut("parameters")
            .ok_or(anyhow::anyhow!("No parameters in storage-class.yaml"))?
            .as_mapping_mut()
            .ok_or(anyhow::anyhow!(
                "parameters is not a mapping in storage-class.yaml"
            ))?
            .insert(
                "numberOfReplicas".into(),
                serde_yaml::Value::String(replicas.to_string()),
            );
        config
            .data
            .as_mut()
            .ok_or(anyhow::anyhow!("No data in configmap"))?
            .insert(
                "storageclass.yaml".into(),
                serde_yaml::to_string(&storage_class_yml)?,
            );
        config_api
            .patch("longhorn-storageclass", &pp, &Patch::Apply(config))
            .await?;
    }
    let storage_class_api: Api<k8s_storage::StorageClass> = Api::all(client.clone());
    let mut storage_class = storage_class_api.get("longhorn").await?;
    while !storage_class.parameters.is_some_and(|p| {
        p.get("numberOfReplicas")
            .map(|v| v == &replicas.to_string())
            .unwrap_or(false)
    }) {
        tracing::info!("Waiting for longhorn storage class to be updated");
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        storage_class = storage_class_api.get("longhorn").await?;
    }

    Ok(())
}
pub async fn enable_v2_data_engine(client: &Client) -> Result<()> {
    let pp = PatchParams {
        force: true,
        field_manager: Some("nirvati".to_string()),
        ..Default::default()
    };
    set_setting(client, "v2-data-engine", "true").await?;
    set_setting(client, "v1-data-engine", "false").await?;
    let config_api: Api<k8s::ConfigMap> = Api::namespaced(client.clone(), "longhorn-system");
    let mut config = config_api.get("longhorn-storageclass").await?;
    config.metadata.managed_fields = None;
    let storage_class_data = config
        .data
        .clone()
        .ok_or(anyhow::anyhow!("No data in configmap"))?;
    let storage_class_yml = storage_class_data
        .get("storageclass.yaml")
        .ok_or(anyhow::anyhow!("No storageclass.yaml in configmap"))?;
    let mut storage_class_yml: serde_yaml::Value = serde_yaml::from_str(storage_class_yml)?;
    storage_class_yml
        .get_mut("parameters")
        .ok_or(anyhow::anyhow!("No parameters in storage-class.yaml"))?
        .as_mapping_mut()
        .ok_or(anyhow::anyhow!(
            "parameters is not a mapping in storage-class.yaml"
        ))?
        .insert("dataEngine".into(), serde_yaml::Value::String("v2".into()));
    config
        .data
        .as_mut()
        .ok_or(anyhow::anyhow!("No data in configmap"))?
        .insert(
            "storageclass.yaml".into(),
            serde_yaml::to_string(&storage_class_yml)?,
        );
    config_api
        .patch("longhorn-storageclass", &pp, &Patch::Apply(config))
        .await?;
    let storage_class_api: Api<k8s_storage::StorageClass> = Api::all(client.clone());
    let mut storage_class = storage_class_api.get("longhorn").await?;
    while !storage_class
        .parameters
        .is_some_and(|p| p.get("dataEngine").map(|v| v == "v2").unwrap_or(false))
    {
        tracing::info!("Waiting for longhorn storage class to be updated");
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        storage_class = storage_class_api.get("longhorn").await?;
    }

    Ok(())
}

async fn get_node(client: &Client) -> Result<Node> {
    let api: Api<Node> = Api::namespaced(client.clone(), "longhorn-system");
    let node = api
        .list(&Default::default())
        .await?
        .items
        .into_iter()
        .next()
        .unwrap();
    Ok(node)
}

pub async fn remove_all_disks(client: &Client) -> Result<()> {
    let api: Api<Node> = Api::namespaced(client.clone(), "longhorn-system");
    let pp = PatchParams {
        force: true,
        field_manager: Some("nirvati".to_string()),
        ..Default::default()
    };
    {
        let mut node = get_node(client).await?;
        let mut disks = node.spec.clone().disks.unwrap_or_default();
        for disk in disks.values_mut() {
            disk.allow_scheduling = Some(false);
        }
        node.spec.disks = Some(disks);
        node.metadata.managed_fields = None;
        node.metadata.uid = None;
        api.patch(
            &node.metadata.name.clone().unwrap(),
            &pp,
            &Patch::Apply(node),
        )
        .await?;
    }
    {
        let mut node = get_node(client).await?;
        tokio::time::sleep(tokio::time::Duration::from_secs(5)).await;
        node.spec.disks = Some(Default::default());
        node.metadata.managed_fields = None;
        node.metadata.uid = None;
        api.patch(
            &node.metadata.name.clone().unwrap(),
            &pp,
            &Patch::Apply(node),
        )
        .await?;
    }
    Ok(())
}
pub async fn add_disks(client: &Client, disk_paths: &[String]) -> Result<()> {
    let api: Api<Node> = Api::namespaced(client.clone(), "longhorn-system");
    let mut node = get_node(client).await?;
    let mut disks = node.spec.clone().disks.unwrap_or_default();
    for disk_path in disk_paths {
        if !disks.values().any(|d| d.path == Some(disk_path.clone())) {
            disks.insert(
                slugify!(disk_path),
                NodeDisks {
                    allow_scheduling: Some(true),
                    disk_type: Some(NodeDisksDiskType::Block),
                    eviction_requested: None,
                    path: Some(disk_path.clone()),
                    storage_reserved: None,
                    tags: None,
                },
            );
        }
    }
    let pp = PatchParams {
        force: true,
        field_manager: Some("nirvati".to_string()),
        ..Default::default()
    };
    node.spec.disks = Some(disks);
    node.metadata.managed_fields = None;
    node.metadata.uid = None;
    api.patch(
        &node.metadata.name.clone().unwrap(),
        &pp,
        &Patch::Apply(node),
    )
    .await?;
    Ok(())
}

pub async fn set_setting(client: &Client, setting: &str, value: &str) -> Result<()> {
    let pp = PatchParams {
        force: true,
        field_manager: Some("nirvat".to_string()),
        ..Default::default()
    };
    let api: Api<Setting> = Api::namespaced(client.clone(), "longhorn-system");
    let mut setting = api.get(setting).await?;
    setting.metadata.managed_fields = None;
    setting.value = value.to_string();
    api.patch(
        setting.metadata.name.clone().unwrap().as_str(),
        &pp,
        &Patch::Apply(setting),
    )
    .await?;
    Ok(())
}
