use anyhow::Result;
use kube::{Api, Client};
use semver::Version;

use crate::kubernetes::apply::apply_any;
use crate::sys_components::shared::{
    download_and_parse_release_asset, get_latest_release_from_repo_semver,
};

pub async fn get_latest_release(installed_version: Version) -> Result<Version> {
    get_latest_release_from_repo_semver(
        Some(installed_version),
        "rancher".to_string(),
        "system-upgrade-controller".to_string(),
    )
    .await
}

pub async fn is_installed(client: &Client) -> Result<bool> {
    let api: Api<k8s_openapi::api::core::v1::Namespace> = Api::all(client.clone());
    let namespaces = api.list(&Default::default()).await?;
    let has_ns = namespaces
        .iter()
        .any(|ns| ns.metadata.name == Some("system-upgrade".into()));
    Ok(has_ns)
}

pub async fn install(client: &Client, version: &Version) -> Result<()> {
    let latest_config = download_and_parse_release_asset(
        "rancher",
        "system-upgrade-controller",
        &format!("v{}.{}.{}", version.major, version.minor, version.patch),
        "system-upgrade-controller.yaml ",
    )
    .await?;

    apply_any(client.clone(), latest_config).await?;
    Ok(())
}
