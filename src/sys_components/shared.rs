use anyhow::{bail, Result};
use itertools::Itertools;
use semver::Version;

use crate::kubernetes::deserialize::multidoc_deserialize;

pub async fn get_latest_release_from_repo_semver(
    base_version: Option<Version>,
    owner: String,
    repo: String,
) -> Result<Version> {
    let octocrab = octocrab::instance();
    let mut page: u32 = 1;
    while page < 11 {
        let releases = octocrab
            .repos(&owner, &repo)
            .releases()
            .list()
            .page(page)
            .per_page(20)
            .send()
            .await?;
        // Filter the releases to only include the ones that are newer than the currently installed version,
        // And still have the same major version
        let found_latest_release = releases
            .items
            .iter()
            .filter_map(|release| {
                let tag = release.tag_name.as_str().trim_start_matches('v');
                let release_version = Version::parse(tag).ok()?;
                if (base_version.as_ref().is_some_and(|installed_version| {
                    release_version.major == installed_version.major
                        && &release_version >= installed_version
                }) || base_version.is_none())
                    && !release.prerelease
                {
                    Some(release_version)
                } else {
                    None
                }
            })
            .sorted()
            .last();
        if let Some(release) = found_latest_release {
            return Ok(release);
        } else if releases.items.len() < 20 {
            bail!("Failed to find latest release!");
        } else {
            page += 1;
        }
    }
    bail!("Failed to find latest release!");
}

/*pub async fn download_and_parse_file(
    owner: &str,
    repo: &str,
    branch: &str,
    path: &str,
) -> Result<Vec<serde_yaml::Value>> {
    let path = format!(
        "https://raw.githubusercontent.com/{}/{}/{}/{}",
        owner, repo, branch, path
    );
    let response = reqwest::get(&path).await?;
    let body = response.text().await?;
    Ok(multidoc_deserialize(&body)?)
}*/

pub async fn download_and_parse_release_asset(
    owner: &str,
    repo: &str,
    branch: &str,
    name: &str,
) -> Result<Vec<serde_yaml::Value>> {
    let path = format!(
        "https://github.com/{}/{}/releases/download/{}/{}",
        owner, repo, branch, name
    );
    let response = reqwest::get(&path).await?;
    let body = response.text().await?;
    Ok(multidoc_deserialize(&body)?)
}
