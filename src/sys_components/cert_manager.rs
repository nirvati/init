use anyhow::Result;
use kube::Client;
use semver::Version;

use crate::kubernetes::apply::apply_any;
use crate::sys_components::shared::{
    download_and_parse_release_asset, get_latest_release_from_repo_semver,
};

pub async fn get_latest_release(base_version: Version) -> Result<Version> {
    get_latest_release_from_repo_semver(
        Some(base_version),
        "cert-manager".to_string(),
        "cert-manager".to_string(),
    )
    .await
}

pub async fn is_installed(client: &Client) -> Result<bool> {
    // Check whether the cert-manager namespace exists
    let api: kube::Api<k8s_openapi::api::core::v1::Namespace> = kube::Api::all(client.clone());
    let namespaces = api.list(&Default::default()).await?;
    let has_ns = namespaces
        .iter()
        .any(|ns| ns.metadata.name == Some("cert-manager".into()));
    Ok(has_ns)
}

pub async fn install(client: &Client, version: &Version) -> Result<()> {
    let latest_config = download_and_parse_release_asset(
        "cert-manager",
        "cert-manager",
        &format!("v{}.{}.{}", version.major, version.minor, version.patch),
        "cert-manager.yaml",
    )
    .await?;
    apply_any(client.clone(), latest_config).await?;
    Ok(())
}
