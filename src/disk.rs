use anyhow::{anyhow, bail, Result};
use libparted::{Device, Disk, DiskType, FileSystemType, Partition, PartitionType};
use std::path::{Path, PathBuf};

pub fn list_block_devices() -> Result<Vec<String>> {
    nix::unistd::sync();
    let mut devices = Vec::new();
    for entry in std::fs::read_dir("/sys/block")? {
        let entry = entry?;
        let path = entry.path();
        let path_str = path.to_str().ok_or(anyhow!("Invalid path"))?;
        if path_str.starts_with("/sys/block/sd")
            || path_str.starts_with("/sys/block/nvme")
            || path_str.starts_with("/sys/block/vd")
            || path_str.starts_with("/sys/block/hd")
            || path_str.starts_with("/sys/block/fd")
            || path_str.starts_with("/sys/block/mmcblk")
        {
            // Strip the /sys/block/ prefix
            devices.push(path_str[11..].to_string());
        }
    }
    Ok(devices)
}

pub fn init_block_device(device: &str) -> Result<()> {
    let device_path = format!("/dev/{}", device);
    // Sync the device to ensure that all data is written
    nix::unistd::sync();
    let mut dev = Device::new(&device_path)?;
    let sector_size = dev.sector_size() as i64;
    let dev_len = dev.length();
    // Crate to create a GPT partition table with one btrfs 1GB partition for the metadata, and one unformatted partition for the longhorn data
    {
        let mut disk = Disk::new_fresh(&mut dev, DiskType::get("gpt").unwrap())?;
        let mut meta_partition = Partition::new(
            &disk,
            PartitionType::PED_PARTITION_NORMAL,
            Some(&FileSystemType::get("btrfs").unwrap()),
            // Start & end
            34i64,
            // End is after 1GB, so 34 (for the header) + 1GB in sectors
            (34 + (1024 * 1024 * 1024) / 512) as i64,
        )?;
        meta_partition.set_name("nirvati")?;
        disk.add_partition(
            &mut meta_partition,
            &disk
                .constraint_any()
                .ok_or(anyhow!("Failed to get disk constraint"))?,
        )?;
        let data_start = (34 + (1024 * 1024 * 1024) / 512) as i64;
        let mut data_end = dev_len.try_into()?;
        // Leave space for the GPT backup header
        data_end -= 34;
        // Ensure that the data partition size is a multiple of 4096 (Longhorn requirement) / 512 (sector size) (8)
        let current_data_size = data_end - data_start;
        let remainder = current_data_size % (4096 / sector_size);
        if remainder != 0 {
            data_end -= remainder;
        }
        let mut data_partition = Partition::new(
            &disk,
            PartitionType::PED_PARTITION_NORMAL,
            None,
            data_start,
            data_end,
        )?;
        data_partition.set_name("nirvati-data")?;
        disk.add_partition(
            &mut data_partition,
            &disk
                .constraint_any()
                .ok_or(anyhow!("Failed to get disk constraint"))?,
        )?;
        disk.commit()?;
    }
    dev.sync()?;
    let output = std::process::Command::new("mkfs.btrfs")
        .arg("-f")
        .arg(format!("/dev/{}", get_partition(device, 1)))
        // Label
        .arg("-L")
        .arg("nirvati")
        .output()?;
    if !output.status.success() {
        let stderr = String::from_utf8(output.stderr)?;
        return Err(anyhow!("Failed to format device: {}", stderr));
    }

    Ok(())
}

#[derive(Debug)]
pub struct PartitionInfo {
    pub label: String,
    pub size: u64,
    pub filesystem: String,
    pub uuid: String,
}

pub fn get_block_device_parts(device: &str) -> Result<Vec<PartitionInfo>> {
    // List all partitions of a block device using libparted
    let mut dev = Device::new(format!("/dev/{}", device))?;
    let disk = Disk::new(&mut dev);
    let Ok(disk) = disk else {
        tracing::error!("Failed to get disk: {}", device);
        return Ok(vec![]);
    };
    Ok(disk
        .parts()
        .map(|part| -> Result<PartitionInfo> {
            // Find the entry in /dev/disk/by-partuuid that symlinks to this partition
            let mut uuid = "".to_string();
            for entry in std::fs::read_dir("/dev/disk/by-partuuid")? {
                let entry = entry?;
                let target = entry.path().read_link()?;
                if target
                    .file_name()
                    .ok_or(anyhow!("Failed to get file name!"))?
                    .to_str()
                    .ok_or(anyhow!("Failed to get file name!"))?
                    .ends_with(get_partition(device, part.num() as u8).as_str())
                {
                    uuid = entry.file_name().into_string().unwrap();
                    break;
                }
            }
            if uuid.is_empty() {
                tracing::error!("Failed to find UUID for partition {}", part.num());
            }
            Ok(PartitionInfo {
                label: part.name().unwrap_or("unknown".to_string()),
                size: part.geom_length().try_into().unwrap(),
                filesystem: part.fs_type_name().unwrap_or("unknown").to_owned(),
                uuid,
            })
        })
        // Skip partitions that have unknown fs & name and have a size of less than 2048
        .filter_map(|part| {
            if let Ok(part) = part {
                if part.filesystem == "unknown" && part.label == "unknown" && part.size < 2048 {
                    None
                } else {
                    Some(part)
                }
            } else {
                tracing::error!("{:?}", part.unwrap_err());
                None
            }
        })
        .collect())
}

pub fn get_partition_parent(part: &str) -> Result<String> {
    let mut partition = part.to_string();
    if partition.starts_with("sd")
        || partition.starts_with("fd")
        || partition.starts_with("hd")
        || partition.starts_with("vd")
    {
        while partition.ends_with(|c: char| c.is_ascii_digit()) {
            partition.pop();
        }
        Ok(partition)
    } else if partition.starts_with("nvme") || partition.starts_with("mmcblk") {
        while partition.ends_with(|c: char| c.is_ascii_digit()) {
            partition.pop();
        }
        if partition.ends_with('p') {
            Ok(partition)
        } else {
            // We did not have a partition, so return the device name
            Ok(part.to_string())
        }
    } else {
        bail!("Unknown partition type!");
    }
}

pub fn resolve_symlink(path: &Path) -> PathBuf {
    let mut path = path.to_path_buf();
    while let Ok(target) = std::fs::read_link(&path) {
        path = target;
    }
    path
}

pub fn get_root_disks() -> Result<Vec<String>> {
    // Get the device name from /proc/partitions
    let mounts = std::fs::read_to_string("/proc/mounts")?;
    let mut root_disk = "".to_string();
    for line in mounts.lines() {
        let mut parts = line.split(' ');
        let device = parts.next().unwrap();
        let mount = parts.next().unwrap();
        if mount == "/host-root" {
            let device = Path::new(device);
            let dev = resolve_symlink(device);
            let dev = dev.file_name().unwrap().to_str().unwrap();
            if dev.starts_with("dm") {
                root_disk = dev.to_string();
            } else {
                root_disk = get_partition_parent(dev)?;
            }
            break;
        }
    }
    let mut root_disks = Vec::new();
    // If it is a dm device, get the real device
    if root_disk.starts_with("dm") {
        let slaves = std::fs::read_dir(format!("/sys/block/{}/slaves", root_disk))?;
        for slave in slaves {
            let slave = slave?;
            let slave = slave.file_name().into_string().unwrap();
            root_disks.push(get_partition_parent(&slave)?);
        }
    } else {
        root_disks.push(root_disk);
    }
    Ok(root_disks)
}

pub fn list_non_root_block_devices() -> Result<Vec<String>> {
    let root_disks = get_root_disks()?;
    tracing::info!("Root disks: {:?}", root_disks);
    let devices = list_block_devices()?
        .into_iter()
        .filter(|d| !root_disks.contains(&get_partition_parent(d).unwrap()))
        .collect::<Vec<String>>();
    Ok(devices)
}

// Gets the name of the path to a partition from a block device
// get_partition("sda", 1) -> sda1
// get_partition("nvme0n1", 1) -> nvme0n1p1
pub fn get_partition(dev: &str, part: u8) -> String {
    if dev.starts_with("nvme") || dev.starts_with("mmcblk") {
        format!("{}p{}", dev, part)
    } else {
        format!("{}{}", dev, part)
    }
}

pub fn initialize_disks() -> Result<Vec<String>> {
    let devices = list_non_root_block_devices()?;
    let mut found_longhorn_disks = Vec::new();
    for device in devices {
        tracing::info!("Initializing disk {}", device);
        // Check if the device has a partition table
        let mut partitions = get_block_device_parts(&device)
            .unwrap_or_else(|_| panic!("Failed to get partitions for {}", device));
        let has_nirvati = partitions.iter().any(|p| p.label == "nirvati")
            && partitions.iter().any(|p| p.label == "nirvati-data");
        if !has_nirvati {
            // Initialize the disk
            init_block_device(&device)?;
        }
        let mut nirvati_data_uuid = "".to_string();
        while nirvati_data_uuid.is_empty() {
            // Wait for the partition to be created
            partitions = get_block_device_parts(&device)?;
            let nirvati_data = partitions
                .iter()
                .find(|p| p.label == "nirvati-data")
                .ok_or(anyhow!("Failed to find nirvati-data partition"))?;
            nirvati_data_uuid = nirvati_data.uuid.clone();
            if nirvati_data_uuid.is_empty() {
                tracing::info!("Waiting for nirvati-data partition to be created");
                std::thread::sleep(std::time::Duration::from_secs(1));
            }
        }

        found_longhorn_disks.push(format!("/dev/disk/by-partuuid/{}", nirvati_data_uuid));
    }
    Ok(found_longhorn_disks)
}
