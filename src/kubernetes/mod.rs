use std::collections::BTreeMap;

use anyhow::Result;
use k8s_openapi::api::core::v1::Namespace;
use k8s_openapi::apimachinery::pkg::apis::meta::v1::ObjectMeta;
use k8s_openapi::ByteString;
use kube::api::{ListParams, PostParams};
use kube::{Api, Client};

pub mod apply;
pub mod deserialize;

pub async fn create_secret(
    client: &Client,
    namespace: &str,
    name: &str,
    data: &BTreeMap<String, String>,
) -> Result<()> {
    let secret = k8s_openapi::api::core::v1::Secret {
        data: Some(
            data.clone()
                .into_iter()
                .map(|(k, v)| (k, ByteString(v.into())))
                .collect(),
        ),
        metadata: ObjectMeta {
            name: Some(name.into()),
            ..Default::default()
        },
        ..Default::default()
    };
    let api: Api<k8s_openapi::api::core::v1::Secret> = Api::namespaced(client.clone(), namespace);
    // If the secret already exists, replace it, otherwise create it
    let lp = ListParams::default()
        .fields(&format!("metadata.name={}", name))
        .timeout(10);
    let secrets = api.list(&lp).await?;
    if let Some(original_secret) = secrets.items.into_iter().next() {
        let mut secret = secret;
        secret.metadata.resource_version = original_secret.metadata.resource_version;
        api.replace(name, &Default::default(), &secret)
            .await
            .map_err(|e| anyhow::anyhow!("Failed to replace secret: {}", e))?;
    } else {
        api.create(&Default::default(), &secret)
            .await
            .map_err(|e| anyhow::anyhow!("Failed to create secret: {}", e))?;
    }
    Ok(())
}


pub async fn create_namespace(client: Client, namespace: &str) -> Result<()> {
    // Create the namespace if it doesn't exist
    let namespace_api: Api<Namespace> = Api::all(client.clone());
    let lp = ListParams {
        field_selector: Some(format!("metadata.name={}", namespace)),
        ..Default::default()
    };
    let currently_deployed_namespaces = namespace_api.list(&lp).await?;
    if currently_deployed_namespaces.items.is_empty() {
        let pp = PostParams::default();
        let namespace = Namespace {
            metadata: ObjectMeta {
                name: Some(namespace.to_string()),
                ..Default::default()
            },
            ..Default::default()
        };
        namespace_api.create(&pp, &namespace).await?;
    }
    Ok(())
}
