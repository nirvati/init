pub fn multidoc_deserialize(data: &str) -> Result<Vec<serde_yaml::Value>, serde_yaml::Error> {
    use serde::Deserialize;
    let mut docs = vec![];
    for de in serde_yaml::Deserializer::from_str(data) {
        docs.push(serde_yaml::Value::deserialize(de)?);
    }
    while docs.first() == Some(&serde_yaml::Value::Null) {
        docs.remove(0);
    }
    while docs.last() == Some(&serde_yaml::Value::Null) {
        docs.pop();
    }
    Ok(docs)
}
