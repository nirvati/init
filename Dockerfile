FROM rust:1.75.0 as rust-builder

RUN cargo install cargo-chef

FROM rust-builder as planner

WORKDIR /app
COPY . /app

RUN cargo chef prepare --recipe-path recipe.json

FROM rust-builder as build-env

RUN apt update && \
    apt install -y libssl-dev pkg-config build-essential cmake libatomic1 libclang-dev protobuf-compiler libparted-dev libseccomp-dev

WORKDIR /app

COPY --from=planner /app/recipe.json recipe.json
RUN cargo chef cook --release --recipe-path recipe.json

COPY . /app

RUN cargo build --release --bin nirvati-init

FROM ubuntu:22.04

RUN apt update && \
    apt install -y ca-certificates libatomic1 libclang1 libparted2 btrfs-progs udev libseccomp2 curl openssl && \
    curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash && \
    apt purge -y curl && \
    apt clean && \
    rm -rf /var/lib/apt/lists/*

COPY --from=build-env /app/target/release/nirvati-init /

CMD ["/nirvati-init"]
